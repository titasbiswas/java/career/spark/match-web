import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Match } from '../_models/Match';
import { FilterCriteria } from '../_models/FilterCriteria';
import { MatchService } from '../_services/match.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent {

  currentMatch: Match;
  matches: Match[];
  filter: FilterCriteria = new FilterCriteria();
  ageRange: number[] = [18, 95];
  csRange: number[] = [1, 99];
  heightRange: number[] = [135, 210];
  dist: number = 30;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private matchService: MatchService,
    private router: Router) { }

  ngOnInit(): void {

    this.currentMatch = JSON.parse(localStorage.getItem('currentMatch'));
    this.loadInitialMatches();

  }

  loadInitialMatches(): void {
    let fc: FilterCriteria = new FilterCriteria();
    if (this.currentMatch.id) {
      fc.currentId = this.currentMatch.id;
    } else {
      fc.currentId = 0;
    }
    this.matchService.getMatches(fc).subscribe(data => {
      this.matches = data;
    });
  }

  filterMatches(event: any, type: string): void {
    this.filter.currentId = this.currentMatch.id;
    if (type === 'age') {
      this.filter.age.min = event[0];
      this.filter.age.max = event[1];
    } else if (type === 'cs') {
      this.filter.compatibilityScore.min = event[0];
      this.filter.compatibilityScore.max = event[1];
    } else if (type === 'height') {
      this.filter.height.min = event[0];
      this.filter.height.max = event[1];
    } else if (type === 'dist') {
      this.filter.locDist.distanceInKm = event;
      this.filter.locDist.lat = this.currentMatch.city.lat;
      this.filter.locDist.lon = this.currentMatch.city.lon;
    }
    this.matchService.getMatches(this.filter).subscribe(data => {
      this.matches = data;
    });
  }

  returnToLanding(): void {

    this.router.navigate(['landing']);
  }

  }
