import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MatchService } from "../_services/match.service";
import { MessageService } from "../_services/message.service";

@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"]
})
export class LandingComponent implements OnInit {
  constructor(
    private router: Router,
    private matchService: MatchService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  getMatchById(id: number) {
    if (id == null || id == 0) {
      this.messageService.add(
        "Please provide a proper numeric id apart from 0 !"
      );
      return;
    }
    this.matchService.getMatchById(id).subscribe(data => {
      localStorage.setItem("currentMatch", JSON.stringify(data));
      this.router.navigate(["matches"]);
    });
  }
}
